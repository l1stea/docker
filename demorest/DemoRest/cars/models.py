from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()


# Create your models here.
class File(models.Model):
  file = models.FileField(blank=False, null=False)
  remark = models.CharField(max_length=20)
  timestamp = models.DateTimeField(auto_now_add=True)




class Car(models.Model):
    vin = models.CharField(verbose_name = 'Vin', db_index = True, unique = True, max_length = 64)
    color = models.CharField(verbose_name = 'Color', max_length = 64)
    brand = models.CharField(verbose_name = 'Brand', max_length = 64)
    CAR_TYPES = (
        (1, 'Седан'),
        (2, 'Хэчбек'),
        (3, 'Универсал'),
        (4, 'Купе'),
    )
    car_type = models.IntegerField(verbose_name = 'Car_type', choices=CAR_TYPES)
    user = models.ForeignKey(User, verbose_name = 'Пользователь', on_delete = models.CASCADE)


class Post(models.Model):
    video = models.FileField(upload_to='filesInput/')
    csv = models.FileField(upload_to='filesInput/')
    csvTimeL = models.CharField(max_length = 50, default = '00:00:02.02')
    csvTimeR = models.CharField(max_length = 50, default = '00:00:02.4')
    urlTimeL = models.CharField(max_length = 50, default = 3)
    urlTimeR = models.CharField(max_length = 50, default = 8)
    siteUrl = models.CharField(max_length = 300)

    def __str__(self):
        return self.siteUrl


class Result(models.Model):
    inputData = models.ForeignKey(Post, on_delete = models.CASCADE)
    resVideo = models.FileField()

    def __str__(self):
        return self.resVideo.url
