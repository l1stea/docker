from django.shortcuts import render
from rest_framework import generics, status, viewsets
from cars.serializers import CarDetailSerializer, CarListSerializer, PostDetailSerializer, FileSerializer
from cars.models import Car
from cars.models import Post
from cars.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import TokenAuthentication


from rest_framework.parsers import JSONParser, FileUploadParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView


from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser


from cars.YES_WORKING import cropp_video
# Create your views here.
class FileView(APIView):
  parser_classes = (MultiPartParser, FormParser)
  def post(self, request, *args, **kwargs):
    file_serializer = FileSerializer(data=request.data)
    if file_serializer.is_valid():
      file_serializer.save()
      cropp_video("Левая_-_Ракурс_1.mov", "30")
      return Response(file_serializer.data, status=status.HTTP_201_CREATED)
    else:
      return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class ExampleView(APIView):
    """
    A view that can accept POST requests with JSON content.
    """
    parser_classes = [JSONParser]

    def post(self, request, format=None):
        return Response({'received data': request.data})


@api_view(['POST'])
@parser_classes([JSONParser])
def example_view(request, format=None):
    """
    A view that can accept POST requests with JSON content.
    """
    return Response({'received data': request.data})

'''
    # views.py
class FileUploadView(APIView):
    parser_classes = (FileUploadParser, )
    def post(self, request, filename, format=None):
        file = request.FILES['file']
        _ = open('/path/' + up_file.name, 'wb+')
        for chunk in up_file.chunks():
            _.write(chunk)
            _.close()
    def put(self, request, filename, format=None):
        file_obj = request.data['file']
        # ...
        # do some stuff with uploaded file
        # ...
        return Response(status=204)
'''

class AssetAdd(APIView):
    parser_classes = (MultiPartParser, FormParser,)

def post(self, request, format=None):
    serializer = PostDetailSerializer(data=request.DATA)
    my_file = request.FILES['file_field_name']
    filename = '/tmp/myfile'
    with open(filename, 'wb+') as temp_file:
        for chunk in my_file.chunks():
            temp_file.write(chunk)

    my_saved_file = open(filename) #there you go



class FileUploadView(APIView):
    parser_classes = (FileUploadParser, )

    def post(self, request, format='jpg'):
        up_file = request.FILES['file']
        destination = open('/Users/Username/' + up_file.name, 'wb+')
        for chunk in up_file.chunks():
            destination.write(chunk)
            destination.close()

        # ...
        # do some stuff with uploaded file
        # ...
        return Response(up_file.name, status.HTTP_201_CREATED)
'''
class PlainTextParser(BaseParser):
    """
    Plain text parser.
    """
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """
        return stream.read()
'''

class VideoViewSet(generics.CreateAPIView):
    serializer_class = PostDetailSerializer

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)


class PostCreateView(generics.CreateAPIView):
    serializer_class = PostDetailSerializer
    parses_classes = (JSONParser, FormParser, MultiPartParser)

class UploadView(APIView):
    parser_classes = (FileUploadParser,)

    def post(self, request):
        file = request.data.get['file',  None]
        import pdb; pdb.set_trace()
        print(file)
        if file:
            return Response({"message": "File is recieved"}, status=200)
        else:
            return Response({"message": "File is missing"}, status=400)



class CarCreateView(generics.CreateAPIView):
    serializer_class = CarDetailSerializer


class CarsListView(generics.ListAPIView):
    serializer_class = CarListSerializer
    queryset = Car.objects.all()
    permission_classes = (IsAdminUser, )

class CarDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CarDetailSerializer
    queryset = Car.objects.all()
    permission_classes = (IsOwnerOrReadOnly, )
