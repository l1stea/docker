from django.contrib import admin
from django.urls import path, include
from cars.views import *


app_name = 'car'
urlpatterns = [
    path('car/create/', CarCreateView.as_view()),
    path('file/', PostCreateView.as_view()),
    path('all/', CarsListView.as_view()),
    path('car/detail/<int:pk>/', CarDetailView.as_view()),
    
    path('upload/', FileView.as_view(), name='file-upload'),
]
